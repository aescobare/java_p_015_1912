/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.javap15.test.jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

/**
 *
 * @author JAVA
 */
public class TestJpa {
    
    public static void main(String[] args) {
        System.out.println("----- Iniciando Prueba ----");
        EntityManagerFactory emf =  Persistence.createEntityManagerFactory("pruebaBiblioteca");
        System.out.println("Unidad de persistencia obtenida");
        EntityManager em = emf.createEntityManager();
        System.out.println("EntityManager creado ...");
        EntityTransaction et = em.getTransaction();
        System.out.println("Transaccion obtenida ...");
        et.begin();
        System.out.println("Iniciando transaccion");
        Libro libro = new Libro("Origin", "Dan Brown", 2016);
        libro.setEditorial("Arquero");
        System.out.println("Libro creado");
        em.persist(libro);
        System.out.println("Libro guardado");
        et.commit();
        System.out.println("Commit ejecutado");
        em.close();
        System.out.println("----- Fin Prueba ----- ");
        
    }
}

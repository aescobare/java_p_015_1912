Las herramientas que usaran para el desarrollo del curso son las siguientes

Opne JDK, la versión OpenJDK 11 (LTS) para windows
https://adoptopenjdk.net/

NetBeans ultima version
https://netbeans.apache.org/download/nb112/index.html

Maven apache-maven-3.6.3-bin.zip
https://maven.apache.org/download.cgi

GIT, Version 2.24.1 para windows
https://git-scm.com/downloads

Postgres version 10.11 para windows
https://www.enterprisedb.com/downloads/postgres-postgresql-downloads
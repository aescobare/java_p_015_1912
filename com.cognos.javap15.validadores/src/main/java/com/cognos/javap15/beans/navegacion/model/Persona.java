/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.javap15.beans.navegacion.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author JAVA
 */
@Named
@SessionScoped
public class Persona implements Serializable{
 
    private String nombre;
    private String apellido;
    private int edad;
    private String sexo;
    private String email;
    private String ip;
    private String nick;

    public String getNombre() {
        System.out.println("GET NOMBRE");
        return nombre;
    }

    public void setNombre(String nombre) {
        System.out.println("SET NOMBRE");
        this.nombre = nombre;
    }

    public String getApellido() {
        System.out.println("GET APELLIDO");
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getEdad() {
        System.out.println("GET EDAD");
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getSexo() {
        System.out.println("GET SEXO");
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getEmail() {
        System.out.println("GET EMAIL");
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIp() {
        System.out.println("GET  IP");
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }
    
    
    
    public List<String> validar(){
        List<String> errores = new ArrayList<String>();
        if(nombre.length() <= 2){
            errores.add("Nombre incrrecto");
        }
        if(apellido.length() <= 2){
            errores.add("Apellido incrrecto");
        }
        return errores;    
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.javap15.beans.navegacion.model;

/**
 *
 * @author aescobar
 */
public class Departamento {
    
    private int codigo;
    private String nombre;
    private String sigla;

    public Departamento() {
    }

    
    
    public Departamento(int codigo, String nombre, String sigla) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.sigla = sigla;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }
    
    
    
    
    
    
}

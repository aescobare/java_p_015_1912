/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.javap15.beans.navegacion.converters;

import com.cognos.javap15.beans.navegacion.bl.DepartamentoBL;
import com.cognos.javap15.beans.navegacion.model.Departamento;
import java.util.List;
import java.util.stream.Collectors;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author JAVA
 */
@FacesConverter("departamentoConverter")
public class DepartamentoConverter implements Converter<Departamento>{

    @Override
    public Departamento getAsObject(FacesContext fc, UIComponent uic, String string) {        
        List<Departamento> deps = DepartamentoBL.obtenerDeps();
        return deps.stream()
                .filter(dep -> dep.getSigla().equals(string))
                .collect(Collectors.toList()).get(0);
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Departamento dep) {
        return dep.getSigla();
    }

    
}

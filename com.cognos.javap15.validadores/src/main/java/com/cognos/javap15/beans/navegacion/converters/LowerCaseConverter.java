/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.javap15.beans.navegacion.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author JAVA
 */
@FacesConverter("lowerCaseConverter")
public class LowerCaseConverter implements Converter<String>{

    @Override
    public String getAsObject(FacesContext fc, UIComponent uic, String string) {
        if(string != null){
            return string.toLowerCase();
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, String string) {
        if(string != null){
            return string.toLowerCase();
        }
        return null;
    }

    
}

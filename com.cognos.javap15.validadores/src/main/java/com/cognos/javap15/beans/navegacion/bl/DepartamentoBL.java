/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.javap15.beans.navegacion.bl;

import com.cognos.javap15.beans.navegacion.model.Departamento;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author aescobar
 */
public class DepartamentoBL {

    public static List<Departamento> obtenerDeps() {
        List<Departamento> deps = new ArrayList<>();
        deps.add(new Departamento(1, "La Paz", "LP"));
        deps.add(new Departamento(2, "Cochabamba", "CB"));
        deps.add(new Departamento(3, "Santa Cruz", "SC"));
        deps.add(new Departamento(4, "Tarija", "TJ"));
        return deps;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.javap15.showcase.jsf.beans;

import com.cognos.javap15.showcase.jsf.beans.model.Persona;
import com.google.common.io.Resources;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 *
 * @author JAVA
 */
@Named
@RequestScoped
public class DataTableBean {
    
    private List<Persona> personas;
    private String textoXHTML;
    private String textoJAVA;
    private String datosPersona;
    
    @PostConstruct
    public void init(){
        textoXHTML = obtenerContenido("inputsecrretxhtml.txt");
        textoJAVA = obtenerContenido("inputsecrretjava.txt");
        
        personas = new ArrayList<>();
        personas.add(new Persona("Henry", "Coarite", 20, "M"));
        personas.add(new Persona("Edson", "Quiñajo", 18, "M"));
        personas.add(new Persona("Henry", "Coarite", 20, "M"));
        
    }
    
    public void cargarPersona(Persona persona){
        datosPersona = persona.getNombre() + " - " + persona.getApellido() + " - " + persona.getSexo();
    }
    
    public String obtenerContenido(String nombreArchivo){
        try {
            URL url = Resources.getResource(nombreArchivo);
            String texto = Resources.toString(url, StandardCharsets.UTF_8);
            System.out.println("ARCHIVO -> " + texto);
            return texto;
        } catch (MalformedURLException ex) {
            System.out.println("URL malformada " + ex);
        } catch (IOException ex) {
            System.out.println("Error al leer archivo " + ex);
        }
        return null;
    
    }

    public String getTextoJAVA() {
        return textoJAVA;
    }

    public void setTextoJAVA(String textoJAVA) {
        this.textoJAVA = textoJAVA;
    }
    
    

    public String getTextoXHTML() {
        return textoXHTML;
    }

    public void setTextoXHTML(String textoXHTML) {
        this.textoXHTML = textoXHTML;
    }

    public List<Persona> getPersonas() {
        return personas;
    }

    public void setPersonas(List<Persona> personas) {
        this.personas = personas;
    }

    public String getDatosPersona() {
        return datosPersona;
    }

    public void setDatosPersona(String datosPersona) {
        this.datosPersona = datosPersona;
    }
    
    

    
    
    
}

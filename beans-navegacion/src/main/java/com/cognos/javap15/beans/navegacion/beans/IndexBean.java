/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.javap15.beans.navegacion.beans;


import com.cognos.javap15.beans.navegacion.model.Persona;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;



/**
 *
 * @author JAVA
 */
@Named
@RequestScoped
public class IndexBean {
    
    @Inject
    Persona persona;
    
    private List<String> errores;
        
    public void procesar(){
        System.out.println("Ingresando al metodo");
        validar();
    }

    private void validar(){
        errores = persona.validar();
    }
    
    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public List<String> getErrores() {
        return errores;
    }

    public void setErrores(List<String> errores) {
        this.errores = errores;
    }
    
    
    
    
    
}
